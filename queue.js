/**
 * Form html and put in proper place
 * @param episodeObject
 */
function formQueue(episodeObject) {
    var str = null;
    $.get(episodeObject.link, function (data) {
        str = data;
        var autoskipNumber = 0;
        var $html = $(str);
        var queue = getQueue($html);
        var title = getTitle($html);
        var lastPost = addFailures(episodeObject.failures, getLastPost($html));
        if (episodeObject.autoskip) {
            autoskipNumber = addAutoSkip(episodeObject.days, lastPost, queue);
        }
        var result = formResult(title, episodeObject.link, episodeObject.days, queue, lastPost, autoskipNumber);
        $('#queues').append(result);
    });
}

/**
 * Get array of players
 * @param $html
 * @returns {Array}
 */
function getQueue($html) {
    var orederStr = $html.find("td:contains('order of priority')").text().substr('order of priority'.length);
    var orderTmp = orederStr.split('\u2192');
    var order = [];
    $.each(orderTmp, function(key, val) {
        order.push(val.trim());
    });
    return order;
}

/**
 * Get episode title
 * @param $html
 * @returns {XMLList|*}
 */
function getTitle($html) {
    var $td = $html.find("td:contains('order of priority')").prev('td');
    return $td.text();
}

/**
 * Get author and date of last post
 * @param $html
 * @returns {{author: (XMLList|*), date: (XMLList|*)}}
 */
function getLastPost($html) {
    var $post = $html.find('.post.endpost');
    return {
        author: $post.find('.pa-author a').text(),
        date: $post.find('a.permalink').text()
    }
}

/**
 * Form output html
 * @param title
 * @param url
 * @param days
 * @param queue
 * @param lastPost
 * @returns {string}
 */
function formResult(title, url, days, queue, lastPost, autoskipNumber) {
    var html = '<div class="queue-block"><h3><a href="'+url+'">'+title+'</a></h3>';
    html += '<div class="note">Дней на отпись: '+days+'</div>';
    html += '<ul>';
    if (autoskipNumber) {
        var current = nextInQueue(current, queue, autoskipNumber);
    } else {
        var current = nextInQueue(lastPost.author, queue, 1);
    }
    $.each(queue, function(key, val) {
        if (current == val) {
            html += '<li><b>&#8594; '+val+'</b>';
            html += ' '+daysAvailable(days, lastPost.date, autoskipNumber);
            if (autoskipNumber) {
                html += ' {Пропустили: '+autoskipNumber+' чел.}';
            }
        } else {
            html += '<li>'+val;
            if (val == lastPost.author) {
                html += ' (' + lastPost.date + ')';
            }
        }
        html += '</li>';
    });
    html += '</ul></div>';
    return html;
}

/**
 * Convert string date to date object
 * @param dateStr
 * @returns {Date}
 */
function getDate(dateStr) {
    var dateArr = dateStr.split(' ');
    var date = new Date();
    if (dateArr[0] == 'Сегодня') {
        date.setHours(0, 0, 0, 0);
        dateArr[0] = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    } else {
        if (dateArr[0] == 'Вчера') {
            date.setHours(-24, 0, 0, 0);
            dateArr[0] = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        }
    }
    date = customDateParse(dateArr[0]+' '+dateArr[1]);
    return date;
}

/**
 * Parse date string into Date object
 * @param dateStr
 * @returns {Date}
 */
function customDateParse(dateStr) {
    var arr = dateStr.split(' ');
    var dayArr = arr[0].split('-');
    var timeArr = arr[1].split(':');
    var date = new Date();
    date.setYear(parseInt(dayArr[0]));
    date.setMonth(parseInt(dayArr[1])-1);
    date.setDate(parseInt(dayArr[2]));
    date.setHours(parseInt(timeArr[0]));
    date.setMinutes(parseInt(timeArr[1]));
    date.setSeconds(parseInt(timeArr[2]));
    return date;
}


/**
 * Available days for current user to write a post
 * @param days
 * @param dateStr
 * @param autoskipNumber
 * @returns {int}
 */
function daysLeft(days, dateStr, autoskipNumber) {
    var date = getDate(dateStr);
    var currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);
    if (!autoskipNumber) {
        autoskipNumber = 0;
    }
    var deltaSec = currentDate.getTime()-date.getTime();
    var deltaDays = Math.floor(deltaSec/1000/60/60/24) - autoskipNumber * days;
    return (days - deltaDays);
}

/**
 * Available days for current user to write a post. Formatted string.
 * @param days
 * @param dateStr
 * @returns {string}
 */
function daysAvailable(days, dateStr, autoskipNumber) {
    var left = daysLeft(days, dateStr, autoskipNumber);
    if (left > 1) {
        return '['+left+' дн]';
    }
    if (left < 2 && left > -1) {
        return '<span class="attention">['+left+' дн]</span>'
    }
    if (left < 0) {
        return '<span class="failure">[задерживает пост: '+(0 - left)+' дн]</span>'
    }
}

/**
 * Change next author after available days loss
 * @param days
 * @param lastPost
 * @param queue
 * @returns {{author: *, date: string}}
 * @constructor
 */
function addAutoSkip(days, lastPost) {
    var currentDate = new Date();
    var postDate = getDate(lastPost.date);
    var skip = 0;
    while (postDate.getTime() + (days * 24 * 60 * 60 * 1000) < currentDate.getTime())  {
        skip++;
        postDate.setTime(postDate.getTime() + (days * 24 * 60 * 60 * 1000));
    }
    return skip;
}

/**
 * Converd date object into string
 * @param date
 * @returns {string}
 */
function getDateString(date) {
    return  date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' '
        + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
}

/**
 * Get next author
 * @param currentTitle
 * @param queue
 * @param interval - number of skipped authors [set 1 to get next]
 * @returns {*}
 */
function nextInQueue(currentTitle, queue, interval) {
    var currentIndex = queue.indexOf(currentTitle);
    var newTitle;
    if (currentIndex + interval <= queue.length - 1) {
        newTitle = queue[currentIndex + interval];
    } else {
        newTitle = queue[(currentIndex + interval) % queue.length];
    }
    return newTitle;
}

/**
 * Add failures to queue if necessary
 * @param failures
 * @param lastPost
 * @returns {object}
 */
function addFailures(failures, lastPost) {
    $.each(failures, function(key, value) {
        var date = getDate(value);
        if (date > getDate(lastPost.date)) {
            lastPost.date = value;
            lastPost.author = key;
            lastPost.fail = true;
        }
    });
    return lastPost;
}

/**
 * Get list of episodes to check from json
 */
function getEpisodes($elem) {
    var $json = $.parseJSON($elem.text());
    $.each($json, function(key, value) {
        formQueue(value)
    })
}

/**
 * Entry point
 */
$(document).ready(function() {
    getEpisodes($('#json'));
});
