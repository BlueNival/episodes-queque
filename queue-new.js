/**
 * Form html and put in proper place
 * @param episodeObjects
 */
function formQueue(episodeObjects, $elem) {
    $.each(episodeObjects, function (key, episodeObject) {
        var str = null;
        $.get(episodeObject.link, function (data) {
            str = data;
            var autoskipNumber = 0;
            var $html = $(str);
            var queue = getQueue($html);
            var title = getTitle($html);
            var lastPost = addFailures(episodeObject.failures, getLastPost($html));
            if (episodeObject.autoskip) {
                autoskipNumber = addAutoSkip(episodeObject.days, lastPost, queue);
            }
            var result = formResult(title, episodeObject.link, episodeObject.days, queue, lastPost, autoskipNumber);
            $elem.append(result);
        });
    });
}

/**
 * Get array of players
 * @param $html
 * @returns {Array}
 */
function getQueue($html) {
    var orederStr = $html.find("td:contains('order of priority')").text().substr('order of priority'.length);
    var orderTmp = orederStr.split('\u2192');
    var order = [];
    $.each(orderTmp, function (key, val) {
        order.push(val.trim());
    });
    return order;
}

/**
 * Get episode title
 * @param $html
 * @returns {XMLList|*}
 */
function getTitle($html) {
    var $td = $html.find("td:contains('order of priority')").prev('td');
    return $td.text();
}

/**
 * Get author and date of last post
 * @param $html
 * @returns {{author: (XMLList|*), date: (XMLList|*)}}
 */
function getLastPost($html) {
    var $post = $html.find('.post.endpost');
    return {
        author: $post.find('.pa-author a').text(),
        date: $post.find('a.permalink').text()
    }
}

/**
 * Form output html
 * @param title
 * @param url
 * @param days
 * @param queue
 * @param lastPost
 * @returns {string}
 */
function formResult(title, url, days, queue, lastPost, autoskipNumber) {
    var html = '<div class="queue-block"><h3><a href="' + url + '">' + title + '</a></h3>';
    html += '<div class="note">���� �� ������: ' + days + '</div>';
    html += '<ul>';
    if (autoskipNumber) {
        var current = nextInQueue(lastPost.author, queue, autoskipNumber);
    } else {
        var current = nextInQueue(lastPost.author, queue, 1);
    }
    $.each(queue, function (key, val) {
        if (current == val) {
            html += '<li><b>&#8594; ' + val + '</b>';
            html += ' ' + daysAvailable(days, lastPost.date, autoskipNumber);
            //if (autoskipNumber) {
            //    html += ' {Пропустили: ' + autoskipNumber + ' чел.}';
            //}
        } else {
            html += '<li>' + val;
            if (val == lastPost.author) {
                html += ' (' + lastPost.date + ')';
            }
        }
        html += '</li>';
    });
    html += '</ul></div>';
    return html;
}

/**
 * Convert string date to date object
 * @param dateStr
 * @returns {Date}
 */
function getDate(dateStr) {
    var dateArr = dateStr.split(' ');
    var date = new Date();
    if (dateArr[0] == '�������') {
        date.setHours(0, 0, 0, 0);
        dateArr[0] = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    } else {
        if (dateArr[0] == '�����') {
            date.setHours(-24, 0, 0, 0);
            dateArr[0] = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        }
    }
    date = customDateParse(dateArr[0] + ' ' + dateArr[1]);
    return date;
}

/**
 * Parse date string into Date object
 * @param dateStr
 * @returns {Date}
 */
function customDateParse(dateStr) {
    var arr = dateStr.split(' ');
    var dayArr = arr[0].split('-');
    var timeArr = arr[1].split(':');
    var date = new Date();
    date.setYear(parseInt(dayArr[0]));
    date.setMonth(parseInt(dayArr[1]) - 1);
    date.setDate(parseInt(dayArr[2]));
    date.setHours(parseInt(timeArr[0]));
    date.setMinutes(parseInt(timeArr[1]));
    date.setSeconds(parseInt(timeArr[2]));
    return date;
}


/**
 * Available days for current user to write a post
 * @param days
 * @param dateStr
 * @param autoskipNumber
 * @returns {int}
 */
function daysLeft(days, dateStr, autoskipNumber) {
    var date = getDate(dateStr);
    var currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);
    if (!autoskipNumber) {
        autoskipNumber = 0;
    }
    var deltaSec = currentDate.getTime() - date.getTime();
    var deltaDays = Math.floor(deltaSec / 1000 / 60 / 60 / 24) - autoskipNumber * days;
    return (days - deltaDays);
}

/**
 * Available days for current user to write a post. Formatted string.
 * @param days
 * @param dateStr
 * @returns {string}
 */
function daysAvailable(days, dateStr, autoskipNumber) {
    var left = daysLeft(days, dateStr, autoskipNumber);
    if (left > 1) {
        return '[' + left + ' ��.]';
    }
    if (left < 2 && left > -1) {
        return '<span class="attention">[' + left + ' дн]</span>'
    }
    if (left < 0) {
        return '<span class="failure">[����������� ����: ' + (0 - left) + ' ��]</span>'
    }
}

/**
 * Change next author after available days loss
 * @param days
 * @param lastPost
 * @param queue
 * @returns {{author: *, date: string}}
 * @constructor
 */
function addAutoSkip(days, lastPost) {
    var currentDate = new Date();
    var postDate = getDate(lastPost.date);
    var skip = 0;
    while (postDate.getTime() + (days * 24 * 60 * 60 * 1000) < currentDate.getTime()) {
        skip++;
        postDate.setTime(postDate.getTime() + (days * 24 * 60 * 60 * 1000));
    }
    return skip;
}

/**
 * Converd date object into string
 * @param date
 * @returns {string}
 */
function getDateString(date) {
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' '
        + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
}

/**
 * Get next author
 * @param currentTitle
 * @param queue
 * @param interval - number of skipped authors [set 1 to get next]
 * @returns {*}
 */
function nextInQueue(currentTitle, queue, interval) {
    var currentIndex = queue.indexOf(currentTitle);
    var newTitle;
    if (currentIndex + interval <= queue.length - 1) {
        newTitle = queue[currentIndex + interval];
    } else {
        newTitle = queue[(currentIndex + interval) % queue.length];
    }
    return newTitle;
}

/**
 * Add failures to queue if necessary
 * @param failures
 * @param lastPost
 * @returns {object}
 */
function addFailures(failures, lastPost) {
    $.each(failures, function (key, value) {
        var date = getDate(value);
        if (date > getDate(lastPost.date)) {
            lastPost.date = value;
            lastPost.author = key;
            lastPost.fail = true;
        }
    });
    return lastPost;
}

/**
 * Get query settings from other page and render query
 * @param url
 * @param elemName
 * @param $elem
 */
function renderQueueJson(url, elemName, $elem) {
    $.get(url, function (data) {
        str = data;
        var $html = $(str);
        var settings = findQueueSettings($html.find(elemName));
        formQueue(settings, $elem)
    });
}

/*** ---------- UI part ------------- ***/

/**
 * Rerender edit page
 * @param url
 */
var renderQueueUI = function (url) {
    if (window.location.pathname == '/edit.php' && getUrlParameter('id') == url) {
        $('#main-reply').css('display', 'none').before('<a id="switch-queue">������������� �� �����</a>').after(formUI());
    }
    $('#switch-queue').click(function() {
        $('#main-reply').toggle('slow');
        $('#queue-ui').toggle('slow');
        if ($(this).text() == '������������� �� �����') {
            $(this).text('������������� �� ���������');
        } else {
            $(this).text('������������� �� �����');
        }
    });
}

var hideQueueSettings = function (episode, post) {
    if (window.location.pathname == '/viewtopic.php' && getUrlParameter('id') == episode) {
        if ($('#p' + post)) {
            placeEpisodes($('#p' + post + ' .post-box'));
            cutQueueSettings($('#p' + post + ' .post-box'));
        }
    }
}

/**
 * Place your queue to some element
 * @param $elem
 */
var placeEpisodes = function ($elem) {
    var settings;
    if (settings = findQueueSettings($elem)) {
        $elem.addClass('queue-container');
        formQueue(settings, $elem);
    }
}

/**
 * Create UI form
 * @returns {*|jQuery}
 */
var formUI = function () {
    var $div = $('<div />').attr('id', 'queue-ui');
    var settings = findQueueSettings($('#main-reply'));
    $.each(settings, function (index, value) {
        $div.append(addQueueFieldset(value));
    })
    var data = {
        link: null,
        days: null,
        autoskip: null
    }
    $div.append(addQueueFieldset(data));
    var $add = $('<a />').addClass('queue-but-link').attr('id', 'add-queue-item').text('�������� ������').click(function() {
        $('#add-queue-item').before(addQueueFieldset(data));
    });
    var $button = $('<a />').addClass('queue-but-link').text('��������� ��������� �������').click(function() {
        setQueueSettings();
    });
    $div.append($add).append($button);
    return $div;
}

/**
 * Add a fieldset for episode
 * @param data
 * @returns {*}
 */
var addQueueFieldset = function (data) {
    var html = '<fieldset class="queue-eposide">' +
        '<table>' +
        '<tr><td>������ �� ������</td><td><input class="field-link" type="textfield" value="' + data.link + '"/></td></tr>' +
        '<tr><td>���������� ���� �� ���</td><td><input class="field-days" type="textfield" value="' + data.days + '"/></td></tr>' +
        '<tr><td>���������� ������� �������������</td>' +
        '<td>' +
        '<label><input type="radio" name="autoskip-' + data.link + '" value="false"';
    if (!data.autoskip) {
        html += ' checked';
    }
    html += '/>���</label>' +
        '<label><input type="radio" name="autoskip-' + data.link + '" value="true" ';
    if (data.autoskip) {
        html += ' checked';
    }
    html += '/>��</label>' +
        '</td></tr></table>' +
    //    '<a class="delete-queue-item" class="queue-but-link">������� ������</a>' +
        '</fieldset>';
    var $tmp = $(html);
    $tmp.append($('<a />').addClass('queue-but-link').text('������� ������').click(function() {
        $(this).closest('fieldset').remove();
    }));
    return $tmp;
}

var setQueueSettings = function() {
    $queues = new Array();
    $('.queue-eposide').each(function() {
        if ($(this).find('.field-link').val() != 'null') {
            if ($(this).find('input[type="radio"]:checked').val() == 'true') {
                var autoskip = true;
            } else {
                var autoskip = false;
            }
            var tmp = {
                link: $(this).find('.field-link').val(),
                days: $(this).find('.field-days').val(),
                failures: {},
                autoskip: autoskip
            }
            $queues.push(tmp);
        }
    })
    var str = JSON.stringify($queues);
    var $area = $('#main-reply');

    var start = $area.val().indexOf('[queue]');
    if (start == -1) {
        $area.val($area.val() + '[queue]' + str + '[/queue]');

    } else {
        var end = $area.val().indexOf('[/queue]') + '[/queue]'.length;
        console.log(str);
        console.log($area.val().substr(0, start) + '[queue]' + str + '[/queue]' + $area.val().substr(end));
        $area.val($area.val().substr(0, start) + '[queue]' + str + '[/queue]' + $area.val().substr(end));
    }
}

/**
 * Get value of get parameter on current page
 * @param sParam
 * @returns {boolean}
 */
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

/**
 * Get queueSettingsFromElement
 * @param $elem
 * @returns {string}
 */
var findQueueSettings = function ($elem) {
    if ($elem.text().indexOf('[queue]') != -1) {
        var start = $elem.text().indexOf('[queue]') + '[queue]'.length;
        var end = $elem.text().indexOf('[/queue]');
        return JSON.parse($elem.text().substr(start, (end - start)).replace(/[\s\n\r]+/g, ' '));
    } else {
        return new Array();
    }
}

/**
 * Delete queue settings from post
 * @param $elem
 */
var cutQueueSettings = function ($elem) {
    var start = $elem.html().indexOf('[queue]');
    var end = $elem.html().indexOf('[/queue]') + '[/queue]'.length;
    $elem.html($elem.html().substr(0, start) + $elem.html().substr(end))
}

///**
// * Entry point, pass id of configuration post
// */
//$(document).ready(function() {
//    renderQueueUI(32535);
//    hideQueueSettings(289, 32535);
//});